#!/bin/sh

# You'll need root priviledges to install.

# Inspired by https://gist.github.com/sturadnidge/4185338

# CentOS and RedHat dependencies:
# yum install gcc kernel-devel make ncurses-devel
#..............................................................................
fetch_build_and_install_libevent() {
	LIBEVENT_LATEST_URL="http://sourceforge.net/projects/levent/files/latest/download?source=files"
	LIBEVENT_TGZ="libevent-stable-latest.tar.gz"

	wget "${LIBEVENT_LATEST_URL}" -O "${LIBEVENT_TGZ}"

	libevent_dir=`tar tzf $LIBEVENT_TGZ | sed -e 's:/.*::' | uniq`

	tar xzf $LIBEVENT_TGZ

	cd "$libevent_dir"
	./configure --prefix=/usr/local
	make && make install
	cd ..
}
#..............................................................................
fetch_build_and_install_tmux() {
	TMUX_TGZ="tmux-latest.tar.gz"
	TMUX_LATEST="http://sourceforge.net/projects/tmux/files/latest/download?source=files"

	wget "${TMUX_LATEST}" -O ${TMUX_TGZ}

	tmux_dir=`tar tzf $TMUX_TGZ | sed -e 's:/.*::' | uniq`

	tar xzf ${TMUX_TGZ}

	cd ${tmux_dir}
	LDFLAGS="-L/usr/local/lib -Wl,-rpath=/usr/local/lib" ./configure --prefix=/usr/local
	make && make install
	cd ..
}
#..............................................................................
usage() {
	echo "Usage: $0 [-l] [-t]"
	echo "    -l    Fetch, build and install libevent."
	echo "    -t    Fetch, build and install tmux (requires libevent)."
	exit 0
}
#..............................................................................
while getopts ":lt" option; do
	case "${option}" in
		l)
			libevent=1
			;;
		t)
			tmux=1
			;;
		*)
			usage
			;;
	esac
done

if [ ! -z "${libevent}" ]; then
	fetch_build_and_install_libevent
fi
if [ ! -z "${tmux}" ]; then
	fetch_build_and_install_tmux
fi
if [ -z "${libevent}" ] && [ -z "${tmux}" ]; then
	usage
fi
